from gpiozero import Button, LED
from time import sleep
import random

led = LED(4)

player_1 = Button(15)
player_2 = Button(14)

time = random.uniform(5, 10)
sleep(time)
led.on()

while True:
    if player_1.is_pressed:
        print("Andy a gagné !")
        break
    if player_2.is_pressed:
        print("Cecile a gagné !")
        break

led.off()
