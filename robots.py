import curses
from gpiozero import LED

led_w = LED(2)
led_g = LED(3)
led_y = LED(4)
led_r = LED(17)

actions = {
    curses.KEY_UP:    led_w.on,
    curses.KEY_DOWN:  led_g.on,
    curses.KEY_LEFT:  led_y.on,
    curses.KEY_RIGHT: led_r.on,
}

things = [led_g, led_r, led_w, led_y]


def main(window):
    next_key = None
    while True:
        curses.halfdelay(1)
        if next_key is None:
            key = window.getch()
        else:
            key = next_key
            next_key = None
        if key != -1:
            # KEY PRESSED
            curses.halfdelay(3)
            action = actions.get(key)
            if action is not None:
                action()
            next_key = key
            while next_key == key:
                next_key = window.getch()
            # KEY RELEASED
            things.off()

curses.wrapper(main)
