from gpiozero import LED, Button
from time import sleep
from random import uniform
from sys import exit

led = LED(4)
right_button = Button(15)
left_button = Button(14)

left_name = input('Nom du premier joueur ')
right_name = input('Nom du second joueur ')

led.on()
sleep(uniform(5, 10))
led.off()


def pressed(button):
	if button.pin.number == 14:
		print(left_name + ' a gagné !')
	else:
		print(right_name + ' a gagné !')
	exit()


right_button.when_pressed = pressed
left_button.when_pressed = pressed
