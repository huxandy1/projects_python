from gpiozero import LED
from time import sleep

blue = LED(10)
green = LED(9)
red = LED(11)

while True:
    red.on()
    sleep(0.5)
    blue.on()
    sleep(0.5)
    green.on()
    sleep(0.5)
    red.off()
    sleep(0.5)
    blue.off()
    sleep(0.5)
    green.off()
